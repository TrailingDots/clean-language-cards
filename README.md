---
title: "Clean Language Question Prompting"
output:
    output_document:
        toc: yes
pagetitle: Clean Language Question Prompting
---

[//]: # (To convert to html: pandoc -s --toc README.md -o README.html)

# `Clean` - Question Card Prompting

This program presents clean language prompts for practicing
Clean Language.

A student of Clean Language could use these random prompts
to enhance their studies.

Prospective users can learn more about Clean Language from the
links at the bottom of this page.

If you are not familiar with Clean Language, please start
at some of the resources at the bottom of this page.

I have personally found this software useful for working
with myself. Thus, I wrote this software for me and I
sincerely hope others find the `clean` code useful
as well.

# Installation:

This code was written in python 3 and but will likely work for python 2.

While written on a linux system, this code will likely work 
on windows.

Place this code somewhere in your $PATH. Do this
by going to your home directory. Most people have a
bin folder. 

On linux, install is simple:

~~~ bash
    cp clean_lang.py ~/bin/clean
    cd ~/bin
    chmod a+x clean
~~~

I don't have windows, but the procedure is likely the same.

Now you can type  "clean"  (without quotes) in any folder
and run the code to access clean language questions.

# Start the program

This program prompts the user with Clean Language questions.
The questions will be presented, after the start question,
in random order.

Start the program by typing `clean` at a terminal prompt. 

Get a new card any time by pressing `<return>` or `<enter>`.

Stop the program with `Ctrl-c` - that is, press the `'ctrl'`
key and the `'c'` key simultaniously.

This code presents each card with the sugested number of
cards for each question. Thusly, you will received some
questions more times than others.

These questions are taken from excellent The Clean Language Institure
discusion on cards. See below for link.

# Links

The main site:
    [The Clean Language Institute](https://cleanlanguagetraining.com)

For more information on these cards, point your browser at:
    [Clean Cards](https://cleanlanguagetraining.com/wp-content/uploads/2017/12/Clean-Qs-cards-only.pdf)

The main site:
    [The Clean Language Institute](https://cleanlanguagetraining.com)


[Learn Clean Language Online](https://cleanlanguagetraining.com) has
an excellent tutorial and plenty of tips.

There are numerous videos of Clean Language on YouTube.com and
other video sites. Please explore these.

Amazon.com and other book sites have many books on Clean Language.

# Enhancements? Suggestions?

Please! Contact me at:  TrailingDots at gmail.com

