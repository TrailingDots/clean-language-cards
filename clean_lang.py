#!/bin/env python3
"""
    Prompts the user with Clean Language questions.
    The questions will be presented, after the start question,
    in random order.

    This code presents each card with the sugested number of
    cards for each question. Thusly, you will received some
    questions more times than others.

    These questions are taken from The Clean Language Institure.

    For more information on these cards, point your browser at:
        https://cleanlanguagetraining.com/wp-content/uploads/2017/12/Clean-Qs-cards-only.pdf

    The main site:
        https://cleanlanguagetraining.com

    Installation:

    This code was written in python 3 and will likely work for python 2.

    While written on a linux system, this code will likely work 
    on windows.

    Place this code somewhere in your $PATH. Do this
    by going to your home directory. Most people have a
    bin folder. Copy this file to that bin folder and
    make it executable.

    To make a file executable on linux: "chmod a+x clean_lang.py"

    To ease your use, on linux:
        cd ~/bin
        chmod a+x clean_lang.py
        ln -s ~/bin/clean_lang.py ~/bin/clean

    Now you can type  "clean"  (without quotes) in any folder
    and access these questions.
"""

import random


class Card():
    """Creates the 'cards' containing questions.
        How many cards?
        What kind of card?
        Question?
    """
    def __init__(self, area, how_many, kind, question):
        self.area = area
        self.how_many = how_many
        self.kind = kind
        self.question = question

    def __getattr__(self, name):
        # Allow dot notation for items in __dict__
        value = getattr(self.__dict__, name)
        return value

    def __str__(self):
        out = f"{self.area}\n{self.kind}\n\n" \
            f"\t\033[1m{self.question}\033[0m\n"
        return out


class Questions():
    """Add all the questions to offer at random
    times. Provide a context as well."""

    def __init__(self):
        self.cards = []
        self.populate()

    def random_question(self):
        "Select a random question."
        return random.choice(self.cards)

    def add_question(self, question):
        """Given a question, add it the number of
        time it is supposed to be in the deck.
        """
        for _ in range(question.how_many):
            self.cards.append(question)

    def populate(self):
        """Routine with all question definitions"""
        acard = Card("starting",
                     1,
                     "Starting Question",
                     "What would you like to have happen?")
        self.add_question(acard)

        acard = Card("Relate across space",
                     2,
                     "Relate across space",
                     "And when () what happens to ()?")
        self.add_question(acard)

        acard = Card("Developing Form",
                     2,
                     "Shape or size?",
                     "And does () have shape or size?")
        self.add_question(acard)

        acard = Card("Relate over time",
                     2,
                     "Then what happened?",
                     "And then what happens?/And what happens next?")
        self.add_question(acard)

        acard = Card("Identity",
                     1,
                     "What would you like to happen?",
                     "And what would () like to have happen?")
        self.add_question(acard)

        acard = Card("Relate over time",
                     1,
                     "What happens just before?",
                     "And what happens just before ()?")
        self.add_question(acard)

        acard = Card("Identity",
                     1,
                     "Like what?",
                     "And that's () like what?")
        self.add_question(acard)

        acard = Card("Developing Form",
                     5,
                     "What kind of?",
                     "And what kind of () is that ()?")
        self.add_question(acard)

        acard = Card("Developing Form",
                     5,
                     "Where/wherabouts?",
                     "And where/whereabouts is that ()?")
        self.add_question(acard)

        acard = Card("Developing Form",
                     5,
                     "Anything else?",
                     "And is there anything else about that()?")
        self.add_question(acard)


if __name__ == "__main__":
    quest = Questions()
    start = quest.cards[0]

    # Initial question. Always ask first:
    input(f"{quest.cards[0]}")

    while True:
        print("\n------------------")
        # Offer question. Wait for return.
        input(f"\n{quest.random_question()}")

